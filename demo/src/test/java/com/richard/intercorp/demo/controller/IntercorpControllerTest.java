package com.richard.intercorp.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.richard.intercorp.demo.models.ResponseTest;
import com.richard.intercorp.demo.models.base.Response;
import com.richard.intercorp.demo.models.dto.ClienteDto;
import com.richard.intercorp.demo.models.dto.KpiClientes;
import com.richard.intercorp.demo.models.service.impl.ClienteServiceImpl;
import com.richard.intercorp.demo.models.service.interf.IClienteService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.io.DataInput;
import java.util.*;

import static org.mockito.ArgumentMatchers.startsWith;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
@SpringBootTest
public class IntercorpControllerTest {
    public static final ClienteDto CLIENTE_DTO = new ClienteDto();

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    ObjectMapper om = new ObjectMapper();


    @Before
    public void init (){
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();

    }

    @Test
    public void list(){
        /*
        //Response clienteDto = new Response();
        //when(iClienteService.findAllV()).thenReturn((clienteDto));
        ResponseEntity<Map<String, Object>> httpResponse = (ResponseEntity<Map<String, Object>>) intercorpController.list();

        //List<ClienteDto> list = (List<ClienteDto>) httpResponse.getBody().get("list");

        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
        Assert.assertNotNull(httpResponse);
        //Assert.assertFalse(httpResponse.getBody().isEmpty());
       //Assert.assertEquals(list.size(), 1);
       */

    }

    @Test
    public void kpideclientes(){
        /*
        MockitoAnnotations.initMocks(this);
        final ClienteDto clienteDto = new ClienteDto();
        when(iClienteService.findAllDto()).thenReturn(Arrays.asList(clienteDto));

        ResponseEntity<Map<String, Object>> httpResponse = (ResponseEntity<Map<String, Object>>) intercorpController.kpideclientes();
        KpiClientes kpiClientes = (KpiClientes) httpResponse.getBody().get("kpiClientes");
        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
        Assert.assertNotNull(httpResponse);
        Assert.assertFalse(httpResponse.getBody().isEmpty());
*/
    }

    @Test
    public void list2() throws Exception {
        MvcResult result = mockMvc
                .perform(get("/v0/api/clients").content(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andReturn();

        String resultContent = result.getResponse().getContentAsString();
        ResponseTest response = om.readValue(resultContent, ResponseTest.class);
        List<Object> objects = (List<Object>) response.getClients();
        for (Object it: objects) {
            Map ap = (LinkedHashMap) it;
            ClienteDto de = om.convertValue(ap,ClienteDto.class);
            System.out.println(de.getFecha_nacimiento());
        }
        Assert.assertEquals(response.getMessage(),"Exito");
        Assert. assertNotNull(response);
        Assert.assertFalse(objects.isEmpty());
    }

}
