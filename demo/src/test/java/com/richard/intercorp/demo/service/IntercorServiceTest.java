package com.richard.intercorp.demo.service;

import com.richard.intercorp.demo.controller.IntercorpController;
import com.richard.intercorp.demo.models.base.Response;
import com.richard.intercorp.demo.models.dao.jpa.IClienteDao;
import com.richard.intercorp.demo.models.entity.Cliente;
import com.richard.intercorp.demo.models.inmutable.Client;
import com.richard.intercorp.demo.models.service.impl.ClienteServiceImpl;
import com.richard.intercorp.demo.models.service.interf.IClienteService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;

public class IntercorServiceTest {
    @Mock
    IClienteDao iClienteDao;

    @InjectMocks
    ClienteServiceImpl iClienteService;


    @Before
    public void init()  {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void getRestaurantsTest()  {
        final Cliente cliente =new  Cliente();
        Mockito.when(iClienteDao.findAll()).thenReturn(Arrays.asList(cliente));

        final Response response = iClienteService.findAllV();
        assertNotNull(response);
        //assertFalse(response.isEmpty());
        //assertEquals(response.size(), 1);

    }
}
