package com.richard.intercorp.demo.models;

import com.richard.intercorp.demo.models.dto.ClienteDto;

import java.util.List;

public class ResponseTest <T> {

    private String message;
    private T  clients;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getClients() {
        return clients;
    }

    public void setClients(T clients) {
        this.clients = clients;
    }
}
