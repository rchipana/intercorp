package com.richard.intercorp.demo.models.util;

import com.richard.intercorp.demo.models.base.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Binding {

    public static Response binding(BindingResult result) {
        Response response = new Response();

        if (result.hasErrors()) {
            List<String> errors = result.getFieldErrors().stream()
                    .map(err -> "El campo' " + err.getField() + "' " + err.getDefaultMessage())
                    .collect(Collectors.toList());
            response.getResp().put("error", errors);
            return response;
        }

        return null;
    }
}
