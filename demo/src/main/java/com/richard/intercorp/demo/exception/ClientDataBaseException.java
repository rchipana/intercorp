package com.richard.intercorp.demo.exception;



public class ClientDataBaseException  extends  RuntimeException{
    public static ClientDataBaseException of (){
        return new ClientDataBaseException();
    }

    public ClientDataBaseException (String customMessage){
        super(customMessage);
    }

    /**
     * Notice I have made this method 'private', this forces developer to use static constructor 'of' above
     * so code will be more 'idiomatic' when instantiating this object.
     *
     * The difference looks like the following:
     * StudentUnderAgeException exception = StudentUnderAgeException.of(someStudent);
     * vs
     * StudentUnderAgeException exception = new StudentUnderAgeException(someStudent);
     *
     * Note: in this particular case, this idiom falls heavily on 'debate', depending of coding styles.
     * @param
     */
    private  ClientDataBaseException (){
        super(String.format("Error %s en base de datos" , "Null" ));
    }
}
