package com.richard.intercorp.demo.models.inmutable;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@JsonSerialize
@Value.Immutable
public  abstract class Client {

    @NotEmpty(message = "el campo nombre no puede estar vacio")
    @NotNull
    @JsonProperty("nombre")
    public  abstract String nombre();

    @NotEmpty(message = "el campo apellido no puede estar vacio")
    @NotNull
    @JsonProperty("apellido")
    public  abstract String apellido();

    @NotNull(message = "el campo edad no puede estar vacio")
    @JsonProperty("edad")
    public  abstract Integer edad();

    @NotNull(message = "el campo fecha_nacimiento no puede estar vacio")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", locale = "pt-PE", timezone = "Peru/East")
    @JsonProperty("fecha_nacimiento")
    public abstract  Date fecha_nacimiento();



}
