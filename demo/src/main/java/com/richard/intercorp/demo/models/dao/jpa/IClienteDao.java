package com.richard.intercorp.demo.models.dao.jpa;

import com.richard.intercorp.demo.models.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IClienteDao extends JpaRepository<Cliente , Long> {
}
