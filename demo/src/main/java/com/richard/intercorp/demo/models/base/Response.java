package com.richard.intercorp.demo.models.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Response <T> {
    private T data;
    private List<String> errors;
    private String mensaje;
    private String error;
    private Map<String , Object> resp = new HashMap<String, Object>();

    public Response() {
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<String> getErrors() {
        if (this.errors == null ){
            this.errors = new ArrayList<>();
        }
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Map<String, Object> getResp() {
        return resp;
    }

    public void setResp(Map<String, Object> resp) {
        this.resp = resp;
    }
}
