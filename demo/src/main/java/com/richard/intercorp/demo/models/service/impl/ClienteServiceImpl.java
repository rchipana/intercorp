package com.richard.intercorp.demo.models.service.impl;

import com.richard.intercorp.demo.exception.ClientDataBaseException;
import com.richard.intercorp.demo.exception.ClientUnderException;
import com.richard.intercorp.demo.models.base.Response;
import com.richard.intercorp.demo.models.dao.jpa.IClienteDao;
import com.richard.intercorp.demo.models.dto.ClienteDto;
import com.richard.intercorp.demo.models.dto.KpiClientes;
import com.richard.intercorp.demo.models.entity.Cliente;
import com.richard.intercorp.demo.models.inmutable.Client;
import com.richard.intercorp.demo.models.service.interf.IClienteService;
import com.richard.intercorp.demo.models.service.interf.IFClientService;
import com.richard.intercorp.demo.models.util.Constantes;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.sql.SQLDataException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class ClienteServiceImpl implements IClienteService {
    public final static int MINIMUM_ACCEPTED_AGE = 21;

    @Autowired
    IClienteDao iClienteDao;

    @Override
    public List<Cliente> findAll() {
        return iClienteDao.findAll();
    }

    @Override
    public Cliente save(Cliente cliente) {
        return iClienteDao.save(cliente);
    }

    @Override
    public KpiClientes getKpiClientes() {
        return null;
    }

    @Override
    public List<ClienteDto> findAllDto() {

            List<Cliente> clientList = iClienteDao.findAll();
            List<ClienteDto> clienteDtoList = clientList.stream()
                    .map(IFClientService.mapperGet)
                    .collect(Collectors.toList());

            return clienteDtoList;


    }

    @Override
    public Cliente save(ClienteDto clienteDto) {


        try {
            Cliente cliente = IFClientService.mapperPost.apply(clienteDto);
            return iClienteDao.save(cliente);
        } catch (Exception e) {

        }
        return null;
    }

    @Override
    public Response saveResponse(ClienteDto clienteDto) {
        LogManager.getLogger("miw").info("---------------->>>>>>>> ServiceOne:Method");

        if (clienteDto.getEdad()< MINIMUM_ACCEPTED_AGE){
            throw ClientUnderException.of(clienteDto);
        }

        Cliente c  = IFClientService.mapperPost.apply(clienteDto);
        c = iClienteDao.save(c);

        Response response = new Response();
        response.getResp().put("message" , Constantes.EXITO);
        response.getResp().put("client" , c.getId());
        return  response;
    }

    @Override
    public Response saveResponse(Client client) {
        /*
        if (client.edad()< MINIMUM_ACCEPTED_AGE){
            throw ClientUnderException.of(client);
        }
        Cliente c  = IFClientService.mapperInmutable.apply(client);

        c = iClienteDao.save(c);

        Response response = new Response();
        response.getResp().put("message" , Constantes.EXITO);
        response.getResp().put("client" , c.getId());
        */

        return null;
    }

    @Override
    public Response findAllV()  {
        List<Cliente> clientList = null;
        clientList = iClienteDao.findAll();

        if (clientList == null){
           throw new NullPointerException();
        }

        List<ClienteDto> lt = clientList.stream()
                .map(cliente -> new ClienteDto(cliente))
                .collect(Collectors.toList());

        Response response = new Response();
        response.getResp().put("message" , Constantes.EXITO);
        response.getResp().put("clients" , lt);
        return response;
    }


}
