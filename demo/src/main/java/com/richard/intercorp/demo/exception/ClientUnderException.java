package com.richard.intercorp.demo.exception;

import com.richard.intercorp.demo.models.dto.ClienteDto;

public class ClientUnderException extends RuntimeException {

    public static ClientUnderException of (ClienteDto client){
        return new ClientUnderException(client);
    }

    public ClientUnderException (String customMessage){
        super(customMessage);
    }

    /**
     * Notice I have made this method 'private', this forces developer to use static constructor 'of' above
     * so code will be more 'idiomatic' when instantiating this object.
     *
     * The difference looks like the following:
     * StudentUnderAgeException exception = StudentUnderAgeException.of(someStudent);
     * vs
     * StudentUnderAgeException exception = new StudentUnderAgeException(someStudent);
     *
     * Note: in this particular case, this idiom falls heavily on 'debate', depending of coding styles.
     * @param client
     */
    private  ClientUnderException (ClienteDto client){
        super(String.format("Client %s with age %d is considered under adult age." , client.getNombre() , client.getEdad()));
    }
}
